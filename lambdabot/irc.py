from enum import Enum
import socket
import time
# TODO: add the sleep commands to Irc class somehow


class Irc:
    irc = socket.socket()

    def __init__(self):
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def send_raw(self, string):
        self.irc.send(bytes(string, "UTF-8"))

    def send(self, channel, msg):
        self.send_raw(f"PRIVMSG{channel} {msg}\n")

    def connect(self, server, port, nick, phrase):
        # Connect
        # print(f"Connecting to: {server}")
        self.irc.connect((server, port))
        self.send_raw(f"USER {nick} {nick} {nick}: {phrase}\n")
        self.send_raw(f"NICK {nick}\n")

    def join(self, chan):
        # Join chan
        self.send_raw(f"JOIN {chan}\n")

    def auth(self, nick, pwd, nickpwd):
        # Auth
        self.send_raw(f"NICKSERV IDENTIFY {nickpwd} {pwd}\n")

    def get_resp(self):
        message = Message(self.irc.recv(2040).decode("UTF-8"))
        if message.kind == "PING":
            self.send_raw(message.pong)
        return message


class Message:
    sender = ""
    nick = ""
    hostname = ""
    kind = ""
    channel = ""
    body = ""

    def __init__(self, resp):
        self.raw = resp
        self.parse()

    def parse(self):
        if self.raw.find("PING") != -1:
            self.kind = "PING"
            self.pong = f"PONG {self.raw.split() [1]}\r\n"
        elif self.raw.find("PRIVMSG") != -1:
            msg = self.raw.split()
            self.sender = msg[0]
            self.nick = msg[0].split("!")[0][1:]
            self.hostname = msg[0].split("@")[1]
            self.kind = msg[1]
            self.channel = msg[2]
            self.body = self.raw.split(self.channel)[1].strip()[1:]
        else:
            pass


class MessageKind(Enum):
    ACTION = "ACTION"
    ERROR = "ERROR"
    INVITE = "INVITE"
    JOIN = "JOIN"
    KICK = "KICK"
    MODE = "MODE"
    NICK = "NICK"
    PART = "PART"
    PING = "PING"
    PONG = "PONG"
    PRIVMSG = "PRIVMSG"
    PRIVNOTICE = "PRIVNOTICE"
    PUBMSG = "PUBMSG"
    PUBNOTICE = "PUBNOTICE"
    QUIT = "QUIT"
    TOPIC = "TOPIC"
