from irc import Irc
import time


# IRC Server Config
server = "irc.zeronode.net"
port = 6667
chan = "#noagenda"
nick = "Andreyevich"
phrase = "A text aggregation bot."

irc = Irc()
irc.connect(server, port, nick, phrase)
time.sleep(5)
irc.join(chan)
while True:
    message = irc.get_resp()
    if message.kind == "PRIVMSG":
        nick = message.nick
        body = message.body
        if nick != "Doug":
            print(f"{body}")
